#Requires Python 2.7
#The floattostring.dll requires 32bit Python to write floating point numbers in a succinct manner,
#but the dll is not required to run this script.
import cPickle, copy, os, struct, sys
from binascii import hexlify
from cStringIO import StringIO


#Adjust input and output folders here.
inputFolder=r"D:\hexing\b2 dump\bundles\ebx"
outputFolder=r"D:\hexing\b2 ebx"

EXTENSION=".txt" # File extension for the result.
SEP="    "       #Adjust the amount of whitespace on the left of the converted file.

# Show offsets to the left.
printOffsets=True #True/False

# Ignore all instances and fields with these names when converting to text:
IGNOREINSTANCES=[]
IGNOREFIELDS=[]


# First run through all files to create a guid table to resolve external file references.
# Then run through all files once more, but this time convert them using the guid table.
def main():
    print "Creating guid table..."
    createGuidTable()
    print "Converting files..."
    dumpText()

##    debug()

guidTable = {}
##############################################################
##############################################################
def createGuidTable():
    for dir0, dirs, ff in os.walk(inputFolder):
        for fname in ff:
            if fname[-4:]!=".ebx": continue
            f=open(lp(dir0+"\\"+fname),"rb")
            relPath=(dir0+"\\"+fname)[len(inputFolder):-4]
            if relPath[0]=="\\": relPath=relPath[1:]
            try:
                ebx=Ebx(f,relPath)
                f.close()
            except ValueError as msg:
                f.close()
                if str(msg).startswith("The file is not ebx: "):
                    print msg
                    continue
                else: asdf
            guidTable[ebx.fileGuid]=ebx.trueFilename

def dumpText():
    for dir0, dirs, ff in os.walk(inputFolder):
        for fname in ff:
            if fname[-4:]!=".ebx": continue
##            print fname
            f=open(lp(dir0+"\\"+fname),"rb")
            relPath=(dir0+"\\"+fname)[len(inputFolder):-4]
            if relPath[0]=="\\": relPath=relPath[1:]
##            ebx=Ebx(f,relPath)
            try:
                ebx=Ebx(f,relPath)
                f.close()
            except ValueError as msg:
                f.close()
                print msg
                if str(msg).startswith("The file is not ebx: "):
                    continue
                else:
                    asdf
            ebx.dump(outputFolder)


def debug():
    ebxs = []
    for dir0, dirs, ff in os.walk(inputFolder):
        for fname in ff:
            if fname[-4:]!=".ebx": continue
            f=open(lp(dir0+"\\"+fname),"rb")
            relPath=(dir0+"\\"+fname)[len(inputFolder):-4]
            if relPath[0]=="\\": relPath=relPath[1:]
##            ebxs.append(Ebx(f,relPath))
            Ebx(f,relPath)
##            Ebx(f,relPath).dump(outputFolder)

##    for ebx in ebxs:
##        ebx.dump(outputFolder)

    for dir0, dirs, ff in os.walk(inputFolder):
        for fname in ff:
            if fname[-4:]!=".ebx": continue
            f=open(lp(dir0+"\\"+fname),"rb")
            relPath=(dir0+"\\"+fname)[len(inputFolder):-4]
            if relPath[0]=="\\": relPath=relPath[1:]
            Ebx(f,relPath).dump(outputFolder)


        
            

def open2(path,mode="rb"):
    if mode=="wb":    
        # Create folders if necessary and return the file handle.

        # First, create one folder level manully because makedirs might fail
        pathParts=path.split("\\")
        manualPart="\\".join(pathParts[:2])
        if not os.path.isdir(manualPart):
            os.makedirs(manualPart)

        # Now handle the rest, including extra long path names.
        folderPath=lp(os.path.dirname(path))
        if not os.path.isdir(folderPath): os.makedirs(folderPath)
    return open(lp(path),mode)

def lp(path): # Long, normalized pathnames.
    if len(path)<=247 or path=="" or path[:4]=='\\\\?\\': return os.path.normpath(path)
    return unicode('\\\\?\\' + os.path.normpath(path))


try:
    from ctypes import *
    floatlib = cdll.LoadLibrary("floattostring")
    def formatfloat(num):
        bufType = c_char * 100
        buf = bufType()
        bufpointer = pointer(buf)
        floatlib.convertNum(c_double(num), bufpointer, 100)
        rawstring=(buf.raw)[:buf.raw.find("\x00")]
        if rawstring[:2]=="-.": return "-0."+rawstring[2:]
        elif rawstring[0]==".": return "0."+rawstring[1:]
        elif "e" not in rawstring and "." not in rawstring: return rawstring+".0"
        return rawstring
except:
    def formatfloat(num):
        return str(num)

def hex2(num):
    """Convert int to 8 digit hex representation."""
    a=hex(num)
    if a[:2]=="0x": a=a[2:]
    if a[-1]=="L": a=a[:-1]
    while len(a)<8: a="0"+a
    return a


######################
######################
unpackLE = struct.unpack
def unpackBE(typ,data): return struct.unpack(">"+typ,data) 

def hasher(keyword): # 32bit FNV-1 hash with FNV_offset_basis = 5381 and FNV_prime = 33.
    hash = 5381
    for byte in keyword:
        hash = (hash*33) ^ ord(byte)
    return hash & 0xffffffff # Use & because Python promotes the num instead of intended overflow.

class FieldDescriptor:
    def __init__(self, varList, keywordDict):
        self.name    = keywordDict[varList[0]]   # LHS for whatever assignment follows.
        self.type    = varList[1] # Data type.
        self.ref     = varList[2] # Certain field types reference a complex with this.
        self.offset  = varList[3] # Payload offset in number section relative to the complex containing it.
        self.offset2 = varList[4] # Unknown.
        if self.name=="$": self.offset-=8
    def __str__(self):
        s = self.name+":"+hex2(self.type)[4:]
        s+=" "+str(self.offset)
        s+="/"+str(self.offset2)
        if self.ref:
            s+=" "+str(self.ref)
        return s
        
class ComplexDescriptor:
    def __init__(self, varList, keywordDict, fieldDescriptors):
        self.name       = keywordDict[varList[0]]
        
        self.firstField = varList[1] # Index of the first field belonging to the complex.
        self.fieldCount = varList[2] # Total number of fields belonging to the complex.
        self.fields     = fieldDescriptors[self.firstField:self.firstField+self.fieldCount]
        
        self.alignment  = varList[3] # If the complex is an instance, its number payload starts on a multiple of this.
        self.type       = varList[4] # Data type.
        self.size       = varList[5] # Total size of the complex in the number payload section.
        self.size2      = varList[6] # Unknown.

        
    def __str__(self):
        s = self.name+":"+hex2(self.type)[4:]
        #s+=" "+str(self.firstField)+":"+str(self.fieldCount)
        s+=" "+str(self.size)+ " A"+str(self.alignment)
        return s
        
    
class InstanceRepeater:
    def __init__(self, varList):
        self.complexIndex = varList[0] # Index of complex used as the instance.
        self.repetitions  = varList[1] # Number of instances of the same complex type.
class ArrayRepeater:
    def __init__(self, varList):
        self.offset       = varList[0] # Offset in array payload section for the first item.
        self.repetitions  = varList[1] # Number of items in the array.
        self.complexIndex = varList[2] # Not necessary for conversion.
class Complex:
    def __init__(self, desc):
        self.desc=desc
class Field:
    def __init__(self, desc, offset):
        self.desc=desc
        self.offset=offset # Track absolute offset of each field in the ebx.

numDict={    
##        0xC12D:("Q",8),0xc0cd:("B",1) ,0x0035:("I",4),0xc10d:("I",4),0xc14d:("d",8),0xc0ad:("?",1),
##         0xc0fd:("i",4),0xc0bd:("b",1),0xc0ed:("h",2), 0xc0dd:("H",2), 0xc13d:("f",4),

         0xc14d:("?",1),    # Bool.
         0xc18d:("B",1),    # Used as an index of some sorts. And sometimes as a bitmask? 
         0xc26d:("f",4),    # Single float.
         0xc2ad:("16s",16), # Guid.
         0x42ed:("8s",8),   # Short guid. For resource files.
         0xc24d:("I",4),    # Possibly 8 bytes, but the values are too small to know.
         0xc1ed:("i",4),    # Maybe unsigned, but ffffffff appears very often.
         0x0065:("I",4),    # Index for Fileguid-guid pair.
         0xc1ad:("H",2),    # Half int.
         0xc20d:("I",4),    # Plain unsigned int. For hashes, flags and sometimes even to count things.
         0xc1cd:("h",2),    # Half int. Maybe unsigned. Unclear purpose.
         0x432d:("Q",8),    # Unsigned long. But it only ever uses 4 bytes.
         }


# 40ed: String.


# Unknown types:
#   c22d Payload is always 0. Probably 8 bytes, no ref.
#   412d Payload is always 0. Probably 8 bytes, no ref.
#   434d Payload is always 0. Probably 16 bytes, no ref.




# d049 4 floats, but it also has a ref



# 0065: 0 ref with 4 bytes


unknowns = {}



class Ebx:
    def __init__(self, f, relPath):
        self.start = f.tell()
        magic=f.read(4)
        if   magic=="\xCE\xD1\xB2\x0F": self.unpack=unpackLE
        elif magic=="\xCE\xD1\xB4\x0F": self.unpack=unpackLE
        elif magic=="\x0F\xB2\xD1\xCE": self.unpack=unpackBE
        else: raise ValueError("Cannot recognize ebx magic: "+relPath)

        self.relPath=relPath
        self.trueFilename="" # An ebx file might contain its filename with proper capitalization.

        # Header.
        (self.metaSize,             # The file is split into meta and payload.
        self.payloadSize,           # File size = metaSize + payloadSize.
        
        self.guidCount,             # Number of external GUIDs.
        self.instanceRepeaterCount, # Total number of instance repeaters.
        self.guidRepeaterCount,     # Instance repeaters with GUID.
        self.unknown,
        self.complexCount,          # Number of complex entries.
        self.fieldCount,            # Number of field entries.
        self.keywordSize,           # Length of keyword section including padding.
        self.stringSize,            # Length of string section including padding.
        self.arrayRepeaterCount,
        self.numberSize) = self.unpack("3I6H3I",f.read(36)) # Length of normal payload section, i.e. the one full of numbers.
        
        # Handles to the three payload sections.
        self.stringOffset = self.start        + self.metaSize
        self.numberOffset = self.stringOffset + self.stringSize
        self.arrayOffset  = self.numberOffset + self.numberSize

        # Remaining meta.
        self.fileGuid = f.read(16)
        while f.tell()%16!=0: f.seek(1,1) # Padding.
        self.externalGuids      = [(f.read(16),f.read(16))  for i in xrange(self.guidCount)]  # 16 bytes file guid, 16 bytes instance guid.
        self.keywordDict        = {hasher(keyword):keyword  for keyword in f.read(self.keywordSize).split("\0")}
        self.fieldDescriptors   = [FieldDescriptor(self.unpack("IHHii",f.read(16)), self.keywordDict)   for i in xrange(self.fieldCount)]
        self.complexDescriptors = [ComplexDescriptor(self.unpack("IIBBHHH",f.read(16)), self.keywordDict, self.fieldDescriptors)   for i in xrange(self.complexCount)]
        self.instanceRepeaters  = [InstanceRepeater(self.unpack("2H",f.read(4)))   for i in xrange(self.instanceRepeaterCount)] 
        while f.tell()%16!=0: f.seek(1,1) # Padding.
        self.arrayRepeaters     = [ArrayRepeater(self.unpack("3I",f.read(12)))   for i in xrange(self.arrayRepeaterCount)]

        # Payload.
        f.seek(self.numberOffset)
        self.internalGuids=[]
        self.instances=[] # (guid, complex)
        nonGuidIndex=0

##        print relPath
##        print "\n\n"
##        for cd in self.complexDescriptors:
##            print cd
##            for fd in cd.fields:
##                print " ",fd
####        asdf



        # Retrieving the payload:
        #   The numbers section contains the primary payload; the others are accessed only in a complementary manner.
        #   Each complex incorporates 0+ fields and knows about its size in the numbers section, but it has no offset given.
        #   A file is just a list of instances, which are the outermost (unnested) complexes.
        #   The order of the instances is given by the instanceRepeaters, so all instances have payload assigned to them.
        #   The payload for nested complexes is equally straightforward; such a complex appears only as RHS for a field (which does have an offset),
        #   and the payload for that complex is read just as any other field value
        #   (the payload of the inner complex is contained within the payload of the parent complex).

        


        self.isPrimaryInstance = True # The first instance is primary.
        for i, instanceRepeater in enumerate(self.instanceRepeaters):
            for repetition in xrange(instanceRepeater.repetitions):
                # Obey alignment of the instance; peek into the complex for that.
                while f.tell()%self.complexDescriptors[instanceRepeater.complexIndex].alignment!=0: f.seek(1,1)

                # Only instances before guidRepeaterCount have a guid.
                if i<self.guidRepeaterCount:
                    instanceGuid=f.read(16)
                else:
                    # Enumerate the instances without guid and assign a big endian int to them.
                    instanceGuid=struct.pack(">I",nonGuidIndex)
                    nonGuidIndex+=1
                self.internalGuids.append(instanceGuid)

                self.instances.append( (instanceGuid, self.ReadComplex(instanceRepeater.complexIndex,f,True)) )
                self.isPrimaryInstance=False

        if self.trueFilename == "":
            self.trueFilename = relPath

        f.seek(self.stringOffset + self.payloadSize) # Seek to eof.
        
        

    def ReadComplex(self, complexIndex, f, isInstance=False):
        """Read payload from f to fill out the complex identified by complexIndex.

        f must be positioned at the start of the complex payload (in the numbers section).
        The files often access the same complexIndex several times,
        and get differing results because f is positioned at different places."""
        
        complexDesc=self.complexDescriptors[complexIndex]
        cmplx=Complex(complexDesc)
        cmplx.offset=f.tell()
                     
        cmplx.fields=[]
        # Alignment 4 instances require subtracting 8 for all field offsets and the complex size.
        obfuscationShift = 8 if (isInstance and cmplx.desc.alignment==4) else 0
        
        for fieldIndex in xrange(complexDesc.firstField, complexDesc.firstField+complexDesc.fieldCount):
            f.seek(cmplx.offset + self.fieldDescriptors[fieldIndex].offset - obfuscationShift)
            cmplx.fields.append(self.ReadField(fieldIndex,f))


##        # Debugging only.
##        for fieldIndex in xrange(complexDesc.firstField, complexDesc.firstField+complexDesc.fieldCount):
##            fieldDesc = self.fieldDescriptors[fieldIndex]
##            if fieldDesc.type not in unknowns:
##                unknowns[fieldDesc.type] = {"ref":set(),"size":set(),"val":set()}
##            unknowns[fieldDesc.type]["ref"].add(fieldDesc.ref)
##
##            if fieldIndex!= complexDesc.firstField+complexDesc.fieldCount-1:
##                nextDesc = self.fieldDescriptors[fieldIndex+1]
##                unknowns[fieldDesc.type]["size"].add(nextDesc.offset-fieldDesc.offset)
##        if complexDesc.fieldCount:
##            unknowns[fieldDesc.type]["size"].add(complexDesc.size-fieldDesc.offset)
        
        
        f.seek(cmplx.offset+complexDesc.size-obfuscationShift)
        return cmplx


    def ReadField(self,fieldIndex,f):
        """Read payload from f to fill out the field identified by fieldIndex."""
        fieldDesc = self.fieldDescriptors[fieldIndex]
        field = Field(fieldDesc,f.tell())

        if fieldDesc.type in (0xd049, 0x0000, 0x9049, 0x0049):
            field.value=self.ReadComplex(fieldDesc.ref,f)

        elif fieldDesc.type in (0x0091,):  # Array.
            arrayRepeater=self.arrayRepeaters[self.unpack("I",f.read(4))[0]]
            arrayComplexDesc=self.complexDescriptors[fieldDesc.ref]

            # The complex is specifically made for arrays. It contains just 1 field. It has size 4 which is never used.
            # So I skip over to the actual field, which is repeatedly taken from the array repeater.

##            print fieldDesc.name, vars(arrayComplexDesc)
            assert arrayComplexDesc.fieldCount==1
            assert arrayComplexDesc.size==4

            arrayDesc = self.fieldDescriptors[arrayComplexDesc.firstField]
            assert arrayDesc.name == "member"
##            if arrayDesc.type not in unknowns:
##                unknowns[arrayDesc.type] = {"ref":set(),"size":set(),"val":set()}
##            unknowns[arrayDesc.type]["ref"].add(arrayDesc.ref)
            
            

##            print hex2(arrayDesc.type)[4:]
            

            f.seek(self.arrayOffset+arrayRepeater.offset)
            arrayComplex=Complex(arrayComplexDesc)
            arrayComplex.fields=[self.ReadField(arrayComplexDesc.firstField,f) for repetition in xrange(arrayRepeater.repetitions)]
            field.value=arrayComplex

        elif fieldDesc.type == 0x40ed:
            # Payload is int offset for string payload section. The value is the nullterminated string from there.
            stringOffset=self.unpack("i",f.read(4))[0]
            if stringOffset==-1:
                field.value="*nullString*"
            else:
                pos0 = f.tell()
                f.seek(self.stringOffset+stringOffset)
                field.value = ""
                while 1:
                    a = f.read(1)
                    if a == "\0": break
                    field.value += a
                f.seek(pos0)
                
                if self.isPrimaryInstance and fieldDesc.name=="Name" and not self.trueFilename:
                    self.trueFilename=field.value
            
                   
        elif fieldDesc.type in (0xc115,):
            # Pick one option out of several.
            # 
            v = f.read(4)
            compareValue=self.unpack("i",v)[0]
            enumComplex=self.complexDescriptors[fieldDesc.ref]

            if enumComplex.fieldCount==0:
                field.value="*nullEnum*"
            for fieldIndex in xrange(enumComplex.firstField,enumComplex.firstField+enumComplex.fieldCount):
##                print vars(self.fieldDescriptors[fieldIndex])
                if self.fieldDescriptors[fieldIndex].offset==compareValue:
                    field.value=self.fieldDescriptors[fieldIndex].name
                    break
            else:
                asdf

##        elif fieldDesc.type==0x417d:
##            field.value=f.read(8)
        
        else:
            try:
                (typ,length)=numDict[fieldDesc.type]
                num=self.unpack(typ,f.read(length))[0]
                field.value=num
            except:
##                print "Unknown field type:",hex2(fieldDesc.type)[4:]
##                print " File name:",self.relPath
##                print " Offset:",field.offset
##                print " Name:", fieldDesc.name
##                print " Ref:",fieldDesc.ref
##                print " Offset:",fieldDesc.offset
##                print " Secondary offset:",fieldDesc.offset2
##                asd
                field.value = "*unknown field type*"#+str(fieldDesc.ref)
##                field.value = hexlify(f.read(16))
        return field
        

    def dump(self,outputFolder):
##        if not self.trueFilename: self.trueFilename=hexlify(self.fileGUID)
        
        outName=outputFolder+self.trueFilename+EXTENSION
        
##        dirName=os.path.dirname(outputFolder+self.trueFilename)
##        if not os.path.isdir(dirName): os.makedirs(dirName)
##        if not self.trueFilename: self.trueFilename=hexlify(self.fileGUID)
##        f2=open(outputFolder+self.trueFilename+EXTENSION,"wb")
##        return
        f2=open2(outName,"wb")
        
        for (guid,instance) in self.instances:
            if instance.desc.name not in IGNOREINSTANCES: #############
                #print 
                writeInstance(f2,instance,hexlify(guid))
                self.recurse(instance.fields,f2,0)
        f2.close()

    def recurse(self, fields, f2, lvl): #over fields
        lvl+=1
        for i, field in enumerate(fields):

##            0xc1ed:("i",4),0x9049:("f",4),0xc14d:("?",1)


            if field.desc.type in (0xd049, 0x0000, 0x9049, 0x0049):
##            if field.desc.type in (0x0029,0xd029,0x0000,0x8029):
                if field.desc.name not in IGNOREFIELDS: #############
                    writeFieldType(f2,field,lvl,field.value.desc.name)
                    self.recurse(field.value.fields,f2,lvl)
            elif field.desc.type==0xc26d:
                writeField(f2,field,lvl,formatfloat(field.value))
            elif field.desc.type==0xc2ad:
                writeField(f2,field,lvl,hexlify(field.value).upper()) #upper case=> chunk guid
            elif field.desc.type == 0x42ed:
                writeField(f2,field,lvl,hexlify(field.value).upper())
            elif field.desc.type == 0x40ed:
                writeField(f2,field,lvl,field.value)
                
##            elif field.desc.type==0x417d:
##                val=hexlify(field.value)
####                val=val[:16]+"/"+val[16:]
##                writeField(f2,field,lvl," "+val)
            elif field.desc.type == 0x0065:
                towrite=""
                if field.value>>31:
                    extguid=self.externalGuids[field.value&0x7fffffff]
                    try: towrite=guidTable[extguid[0]]+"/"+hexlify(extguid[1])
                    except: towrite=hexlify(extguid[0])+"/"+hexlify(extguid[1])
                elif field.value==0: towrite="*nullGuid*"
                else:
                    intGuid=self.internalGuids[field.value-1]
                    towrite=hexlify(intGuid)
                writeField(f2,field,lvl," "+towrite) 
            elif field.desc.type in (0x0091,):
                if len(field.value.fields)==0:
                    writeField(f2,field,lvl,"*nullArray*")
                else:
                    writeFieldType(f2,field,lvl,field.value.desc.name)
                    
                    #

                    # Quick hack so I can add indices to array members while using the same recurse function.
                    for index in xrange(len(field.value.fields)):
                        member=field.value.fields[index]
                        if member.desc.name=="member":
                            desc=copy.deepcopy(member.desc)
                            desc.name="member("+str(index)+")"
                            member.desc=desc
                    self.recurse(field.value.fields,f2,lvl)
            elif field.desc.type in numDict:
                writeField(f2,field,lvl,str(field.value))
                
            else:
##                print vars(field)
##                fieldSize = sorted(unknowns[field.desc.type]["size"])[0]*2
##                val = str(field.value)[:fieldSize]
##                unknowns[field.desc.type]["val"].add(val)
##                writeField(f2,field,lvl,val+" "+str(field.desc.ref)+" "+hex2(field.desc.type)[4:])
                
                writeField(f2,field,lvl,str(field.value))

if printOffsets:
    def writeField(f,field,lvl,text):
        f.write(hex2(field.offset)+SEP+lvl*SEP+field.desc.name+" "+text+"\r\n")
    def writeFieldType(f,field,lvl,text):
        f.write(hex2(field.offset)+SEP+lvl*SEP+field.desc.name+"::"+text+"\r\n")
    def writeInstance(f,cmplx,text):
        f.write(hex2(cmplx.offset)+SEP+cmplx.desc.name+" "+text+"\r\n")     
else:
    def writeField(f,field,lvl,text):
        f.write(lvl*SEP+field.desc.name+" "+text+"\r\n")
    def writeFieldType(f,field,lvl,text):
        f.write(lvl*SEP+field.desc.name+"::"+text+"\r\n")
    def writeInstance(f,cmplx,text):
        f.write(cmplx.desc.name+" "+text+"\r\n")


if outputFolder[-1] not in ("/","\\"): outputFolder+="\\"
if inputFolder[-1] not in ("/","\\"): inputFolder+="\\"


if __name__ == "__main__":
    main()




### Debugging.
##for k in sorted(unknowns):
##    v = unknowns[k]
####    print hex2(k)[4:]
####    print " ref",("0" if 0 in v["ref"] else "")+("1" if v["ref"]!=set([0]) else "")
####
##    sizes = v["size"]#|v["size3"]
####    print " size"," ".join([str(i) for i in sorted(sizes)])
##
##
##    print hex2(k)[4:], ("0" if 0 in v["ref"] else "")+("1" if v["ref"]!=set([0]) else ""),"", " ".join([str(i) for i in sorted(sizes)])
##    print " "," ".join(list(v["val"])[:50])
##    print " "," ".join(sorted(v["val"])[:50])
##    print " "," ".join(sorted(v["val"])[-50:])
##    
##
### Padding causes inconsistent sizes.
### The true size is <= the smallest size.

















